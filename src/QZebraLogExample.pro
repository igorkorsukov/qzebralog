

TEMPLATE = app
QT = core
TARGET = QZebraLogExample
CONFIG   += console
CONFIG   -= app_bundle

include(paths.pri)
DESTDIR = $$BIN_PATH

include(logging/logger.pri)

SOURCES += main.cpp

