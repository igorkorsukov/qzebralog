#include <QCoreApplication>
#include <QTextCodec>
#include "logging/log.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    QString logPath = qApp->applicationDirPath() + "/logs";
    QString logName = "QZebraLog";
    Logger *logger = Logger::instance();
    logger->addLogDest(new OutputDest(LogLayout("${time} | ${level} | ${tag} | [${code}] ${message}")));
    logger->addLogDest(new FileLogDest(logPath, logName, LogLayout("${datetime} | ${level} | ${tag} | [${code}] ${message}")));
    logger->setLevel(Logger::Full);

    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);

    LOG(Info, "Main", "MT0001") << "You can use this: 'LOG(type) << message' \n"
                                   "Types:\n"
                                   "Debug \n"
                                   "Warning \n"
                                   "Critical \n"
                                   "Fatal \n"
                                   "Info \n"
                                   "CriticalSql \n"
                                   "DebugSql \n"
                                   "Time";
    LOG(Info, "Main", "MT0002") << "or use these macros LOGX(), they are arranged as follows: 'LOG(type) << Q_FUNC_INFO'";

    LOGD_TC("DTag", "MT0000") << "This is LOGD_TC message ";
    LOGD_T("DTag") << "This is LOGD_T message";
    LOGD() << "This is LOGD message";
    LOGD_FI() << "This is LOGD_FI message";

    LOGW() << "This is Warning message";
    LOGE() << "This is Critical(error) message";
    //LOGF() << "This is Fatal message, exit from app!";
    LOGI() << "This is Info message";
    LOGSQL() << "This is DebugSql message (usually Sql query string)";
    LOGTIME() << "This is Time message";

    qDebug() << "Русский текст";

    LOG(Info, "Main", "MT0003") << "or use standard Qt function (qDebug(), qWarning(), qCritical(), qFatal())"
                                   "if Logger::isCatchQtMsg() set true";
    qDebug() << "This is Debug message";
    qWarning() << "This is Warning message";
    qCritical() << "This is Critical message";
    //qFatal("This is Fatal message, exit from app!");

    LOG(Info, "Main", "MT0004") << "or you can create your own macros or functions (See logger.h). Good luck!";

    return a.exec();
}
