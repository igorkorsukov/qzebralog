#ifndef LOGMSG_H
#define LOGMSG_H

#include <QString>
#include <QDateTime>

namespace QZebraLog
{

class LogMsg
{
public:

    enum MsgType {
        TypeDebug,
        TypeWarning,
        TypeError,
        TypeFatal,
        TypeInfo,
        TypeTime,
        TypeSql,
        TypeTrace
    };

    LogMsg(MsgType l, const QString &t, const QString &c)
        : type(l), tag(t), code(c), dateTime(QDateTime::currentDateTime()) {}

    LogMsg(MsgType l, const QString &t, const QString &c, const QString &m)
        : type(l), tag(t), code(c), message(m), dateTime(QDateTime::currentDateTime()) {}


    inline QString typeString(MsgType t) const {
        switch (t) {
        case TypeTrace:         return "TRACE";
        case TypeDebug:         return "DEBUG";
        case TypeInfo:          return "INFO ";
        case TypeWarning:       return "WARN ";
        case TypeError:      return "ERROR";
        case TypeFatal:         return "FATAL";
        case TypeTime:          return "TIME ";
        case TypeSql:           return "SQL  ";
        default:                return "UNDEF";
        }
    }

    QString typeString() const {
        return typeString(type);
    }

    MsgType type;
    QString tag;
    QString code;
    QString message;
    QDateTime dateTime;
};

}

#endif // LOGMSG_H
