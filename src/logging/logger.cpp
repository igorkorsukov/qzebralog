#include "logger.h"
#include <QTextCodec>

using namespace QZebraLog;

Logger::Logger()
    : m_level(WriteError | WriteWarning | WriteInfo)
{
    qInstallMsgHandler(logMsgHandler);
}

Logger::~Logger()
{
    qDeleteAll(m_destList);
}

void Logger::logMsgHandler(enum QtMsgType defType, const char * msg)
{
    LogMsg logMsg(qtTypeToLogType(defType), "Qt", "", QString(msg));
    Logger::instance()->write(logMsg);
}

void Logger::write(const LogMsg &logMsg)
{
    QMutexLocker locker(&m_mutex);
    switch (logMsg.type) {
    case LogMsg::TypeDebug: if(!isEnbLevel(WriteDebug)) return; break;
    case LogMsg::TypeWarning: if(!isEnbLevel(WriteWarning)) return; break;
    case LogMsg::TypeError: if(!isEnbLevel(WriteError)) return; break;
    case LogMsg::TypeFatal: break;
    case LogMsg::TypeInfo: if(!isEnbLevel(WriteInfo)) return; break;
    case LogMsg::TypeTime: if(!isEnbLevel(WriteTime)) return; break;
    case LogMsg::TypeSql: if(!isEnbLevel(WriteSql)) return; break;
    case LogMsg::TypeTrace: if(!isEnbLevel(WriteTrace)) return; break;
    default: return;
    }

    foreach (LogDest *dest, m_destList) {
        dest->write(logMsg);
    }
}

void Logger::addLogDest(LogDest *dest)
{
    assert(dest);
    m_destList.append(dest);
}

void Logger::setLevel(Level level)
{
    m_level = level;
}

Logger::Level Logger::level() const
{
    return m_level;
}

void Logger::setLevel(Level level, bool enb)
{
    setLevel(enb ? m_level | level : m_level &= ~level);
}

void Logger::setIsCatchQtMsg(bool arg)
{
    QtMsgHandler h = arg ? logMsgHandler : 0;
    qInstallMsgHandler(h);
}

