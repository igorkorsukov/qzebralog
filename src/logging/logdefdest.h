#ifndef DEFLOGDEST_H
#define DEFLOGDEST_H

#include "logdest.h"
#include <QFile>
#include <QTextStream>

namespace QZebraLog
{

class FileLogDest : public LogDest
{
public:
    FileLogDest(const QString &path, const QString &name, const LogLayout &l);
    void write(const LogMsg &logMsg);

private:
    void rotate();

    QFile m_file;
    QString m_path;
    QString m_name;
    QTextStream m_stream;
    QDate m_rotateDate;
};

class OutputDest : public LogDest
{
public:
    OutputDest(const LogLayout &l);
    void write(const LogMsg &logMsg);
};

}

#endif // DEFLOGDEST_H
