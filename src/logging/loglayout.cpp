#include "loglayout.h"

namespace QZebraLog
{

LogLayout::LogLayout(const QString &f)
    : m_format(f)
{
}

QString LogLayout::output(const LogMsg &logMsg)
{
    // NOTE: now is just simple
    QString str = m_format;
    // NOTE: Format date and time is slow, need check contains
    if (m_format.contains("${datetime}")) str.replace("${datetime}", dateTimeFormat(logMsg.dateTime));
    if (m_format.contains("${date}")) str.replace("${date}", dateFormat(logMsg.dateTime.date()));
    if (m_format.contains("${time}")) str.replace("${time}", timeFormat(logMsg.dateTime.time()));
    str.replace("${level}", logMsg.typeString());
    str.replace("${tag}", logMsg.tag);
    str.replace("${code}", logMsg.code);
    str.replace("${message}", logMsg.message);

    return str;
}

void LogLayout::setFormat(const QString &f)
{
    m_format = f;
}

QString LogLayout::format() const
{
    return m_format;
}

QString LogLayout::dateTimeFormat(const QDateTime &dt)
{
    QString str(dateFormat(dt.date()));
    str.append("T");
    str.append(timeFormat(dt.time()));

    return str;
}

QString LogLayout::dateFormat(const QDate &d)
{
    return d.toString(Qt::ISODate);
}

QString LogLayout::timeFormat(const QTime &t)
{
    QString str(t.toString(Qt::ISODate));
    str.append(".");
    str.append(QString::number(t.msec()).rightJustified(3, QLatin1Char('0'), true));

    return str;
}

}
