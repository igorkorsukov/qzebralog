#include "logdefdest.h"
#include <QDir>
#include <QCoreApplication>

using namespace QZebraLog;
// FileLogDest
FileLogDest::FileLogDest(const QString &path, const QString &name, const LogLayout &l)
    : LogDest(l), m_path(path), m_name(name), m_stream(&m_file)
{
    rotate();
}

void FileLogDest::write(const LogMsg &logMsg)
{
    if (m_rotateDate != logMsg.dateTime.date())
        rotate();

    m_stream << m_layout.output(logMsg) << "\r\n";
    m_stream.flush();
}

void FileLogDest::rotate()
{
    if (m_file.isOpen())
        m_file.close();

    QDir dir(m_path);
    if (!dir.exists() && !dir.mkpath(m_path)) {
        fprintf(stderr, "Debug: FileLogDest can not mkpath %s\n", qPrintable(m_path));
        fflush(stderr);
        return;
    }

    m_rotateDate = QDate::currentDate();
    QString fileName = QString("%1/%2-%3.log").arg(m_path).arg(m_name).arg(m_rotateDate.toString("yyMMdd"));
    m_file.setFileName(fileName);
    if (!m_file.open(QFile::Append)) {
        fprintf(stderr, "Debug: FileLogDest can not open %s\n", qPrintable(fileName));
        fflush(stderr);
        return;
    }
}


// OutputDest
OutputDest::OutputDest(const LogLayout &l) : LogDest(l)
{

}

#if defined(Q_OS_WIN)
#include "qt_windows.h"
void OutputDest::write(const LogMsg &logMsg)
{
    static CRITICAL_SECTION criticalSection;
    InitializeCriticalSection(&criticalSection);
    EnterCriticalSection(&criticalSection);

    OutputDebugString(reinterpret_cast<const WCHAR*>(m_layout.output(logMsg).utf16()));
    OutputDebugString(L"\n");

    LeaveCriticalSection(&criticalSection);
    DeleteCriticalSection(&criticalSection);
}

#elif defined(Q_OS_SYMBIAN)
#include <e32debug.h>
void OutputDest::write(const LogMsg &logMsg)
{
    // RDebug::Print has a cap of 256 characters so break it up
    char format[] = "[Qt Message] %S\n";
    const int maxBlockSize = 256 - sizeof(format);
    const TPtrC8 ptr(reinterpret_cast<const TUint8*>(qPrintable(m_layout.output(logMsg))));
    for (int i = 0; i < ptr.Length(); i += maxBlockSize) {
        TPtrC8 part(ptr.Mid(i, qMin(maxBlockSize, ptr.Length()-i)));
        RDebug::Printf(format, &part);
    }
}

#elif defined (Q_OS_ANDROID)
#include <android/log.h>
#define  LOG_TAG qApp->applicationName()
void OutputDest::write(const LogMsg &logMsg)
{
    int TYPE = ANDROID_LOG_DEFAULT;
    switch (logMsg.type) {
    case LogMsg::TypeDebug: TYPE = ANDROID_LOG_DEBUG; break;
    case LogMsg::TypeWarning: TYPE = ANDROID_LOG_WARN; break;
    case LogMsg::TypeCritical: TYPE = ANDROID_LOG_ERROR; break;
    case LogMsg::TypeFatal: TYPE = ANDROID_LOG_FATAL; break;
    case LogMsg::TypeCriticalSql: TYPE = ANDROID_LOG_ERROR; break;
    case LogMsg::TypeInfo: TYPE = ANDROID_LOG_INFO; break;
    case LogMsg::TypeTime: TYPE = ANDROID_LOG_DEBUG; break;
    case LogMsg::TypeDebugSql: TYPE = ANDROID_LOG_DEBUG; break;
    default: break;
    }

    __android_log_print(TYPE, qPrintable(LOG_TAG), qPrintable(logMsg.message));
}

#else
#include <cstdio>
void OutputDest::write(const LogMsg &logMsg)
{
    fprintf(stderr, "%s\n", qPrintable(m_layout.output(logMsg)));
    fflush(stderr);
}

#endif



