#ifndef LOGDESTINATION_H
#define LOGDESTINATION_H

#include "logmsg.h"
#include "loglayout.h"

namespace QZebraLog
{

class LogDest
{

public:
    LogDest(const LogLayout &l) : m_layout(l) {}

    virtual void write(const LogMsg &logMsg) = 0;

    LogLayout layout() const {
        return m_layout;
    }

protected:
    LogLayout m_layout;
};

}

#endif // LOGDESTINATION_H
