#ifndef LOGGER_H
#define LOGGER_H

#include <QDebug>
#include <QVector>
#include <QMutex>
#include <cassert>
#include "logdest.h"

//#define LOG_TAG "LOG_TAG"
#define LOG_TAG_UNDEF "Undef"
#define LOG_CODE_UNDEF "0000"

#define LOG(type, tag, code) QZebraLog::LogStream(LogMsg::Type##type, tag, code).stream()
#define LOG_FUNC(type) LOG(type, Q_FUNC_INFO, "Dev")

#define LOGD_TC(tag, code) LOG(Debug, tag, code)
#define LOGD_T(tag) LOG(Debug, tag, "0000")
#define LOGD_FI() LOG(Debug, "Dev", Q_FUNC_INFO)

//#define LOGD() LOG_FUNC(Debug)
#define LOGW_TC(tag, code) LOG(Warning, tag, code)

#ifdef LOG_TAG
#define LOGD_C(code) LOG(Debug, LOG_TAG, code)
#define LOGW_C(code) LOG(Warning, LOG_TAG, code)
#define LOGE_C(code) LOG(Error, LOG_TAG_UNDEF, code)
#define LOGF_C(code) LOG(Fatal, LOG_TAG_UNDEF, code)
#define LOGI_C(code) LOG(Info, LOG_TAG_UNDEF, code)
#define LOGSQL_C(code) LOG(Sql, LOG_TAG_UNDEF, code)
#define LOGTIME_C(code) LOG(Time, LOG_TAG_UNDEF, code)
#define LOGTRACE_C(code) LOG(Trace, LOG_TAG_UNDEF, code)
#else
#define LOGD() LOG(Debug, LOG_TAG_UNDEF, LOG_CODE_UNDEF)
#define LOGW() LOG(Warning, LOG_TAG_UNDEF, LOG_CODE_UNDEF)
#define LOGE() LOG(Error, LOG_TAG_UNDEF, LOG_CODE_UNDEF)
#define LOGF() LOG(Fatal, LOG_TAG_UNDEF, LOG_CODE_UNDEF)
#define LOGI() LOG(Info, LOG_TAG_UNDEF, LOG_CODE_UNDEF)
#define LOGSQL() LOG(Sql, LOG_TAG_UNDEF, LOG_CODE_UNDEF)
#define LOGTIME() LOG(Time, LOG_TAG_UNDEF, LOG_CODE_UNDEF)
#define LOGTRACE() LOG(Trace, LOG_TAG_UNDEF, LOG_CODE_UNDEF)
#endif



namespace QZebraLog {

class Logger
{
public:
    static Logger* instance() {
        static Logger l;
        return &l;
    }
    typedef QVector<LogDest*> DestinationList;

    enum LevelType {
        None = 0x00,
        WriteError = 0x01,
        WriteWarning = 0x02,
        WriteInfo = 0x04,
        WriteDebug = 0x08,
        WriteTime = 0x10,
        WriteSql = 0x20,
        WriteTrace = 0x40,
        Full = 0xFF
    };

    Q_ENUMS(LevelType)
    Q_FLAGS(Level)
    Q_DECLARE_FLAGS(Level, LevelType)

    void write(const LogMsg &logMsg);

    void setLevel(Level level);
    Level level() const;
    void setLevel(Level level, bool enb);
    inline bool isEnbLevel(LevelType f) const {return m_level.testFlag(f);}

    void setIsCatchQtMsg(bool arg);

    void addLogDest(LogDest *dest);
    void clearLogDest();

    static inline LogMsg::MsgType qtTypeToLogType(QtMsgType type) {
        return static_cast<LogMsg::MsgType>(type);
    }

private:
    Logger();
    ~Logger();
    static void logMsgHandler (enum QtMsgType defType, const char * msg);

    DestinationList m_destList;
    QMutex m_mutex;
    Level m_level;
};

class LogStream
{
public:
    explicit LogStream(LogMsg::MsgType type, const QString &tag, const QString &code)
        : m_msg(type, tag, code), m_stream(&m_msg.message) {}

    ~LogStream() {
        try {
            Logger::instance()->write(m_msg);
        } catch(std::exception&) {
            assert(!"exception in logger helper destructor");
            throw;
        }
    }

    QDebug& stream() { return m_stream; }

private:
    LogMsg m_msg;
    QDebug m_stream;
};

}

#endif // LOGGER_H
