#ifndef LOGLAYOUT_H
#define LOGLAYOUT_H

#include <QString>
#include "logmsg.h"

namespace QZebraLog
{

class LogLayout
{
public:
    LogLayout(const QString &f);

    QString output(const LogMsg &logMsg);

    void setFormat(const QString &f);
    QString format() const;

    static QString dateFormat(const QDate &d);
    static QString timeFormat(const QTime &t);
    static QString dateTimeFormat(const QDateTime &dt);

private:
    QString m_format;
};

}
#endif // LOGLAYOUT_H
