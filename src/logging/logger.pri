OTHER_FILES += \
    logging/logger.pri

HEADERS += \
    logging/logger.h \
    logging/logdest.h \
    logging/logdefdest.h \
    logging/log.h \
    logging/loglayout.h \
    logging/logmsg.h

SOURCES += \
    logging/logger.cpp \
    logging/logdefdest.cpp \
    logging/loglayout.cpp
