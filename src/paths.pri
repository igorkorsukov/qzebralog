
OBJ_PATH = $$PWD/../obj #Объектные файлы
LIB_PATH = $$PWD/../lib #Статические библиотеки
BIN_PATH = $$PWD/../bin #Исполняемые файлы
PREFIX = ""
android-g++ {
    DEFINES += "PLATFORM=\\\"Android\\\""
    DEFINES += ANDROID
    CONFIG += debug_and_release

    BUILD_DIR = android
    DEFINES += QT_NO_DEBUG_OUTPUT QT_NO_WARNING_OUTPUT
}

win32: {
    DEFINES+="PLATFORM=\\\"Windows\\\""

    CONFIG(debug, debug|release): {
        BUILD_DIR = win32_debug
        PREFIX = "d"
    }
    CONFIG(release, debug|release): {
        BUILD_DIR = win32_release
    }
}

unix:!android-g++ {
    DEFINES+="PLATFORM=\\\"Unix\\\""

    CONFIG(debug, debug|release): {
        BUILD_DIR = linux_debug
        PREFIX = "d"
    }
    CONFIG(release, debug|release): {
        BUILD_DIR = linux_release
    }
}

LIB_PATH = $$LIB_PATH/$$BUILD_DIR
BIN_PATH = $$BIN_PATH/$$BUILD_DIR
OBJECTS_DIR = $$OBJ_PATH/$$BUILD_DIR/$$TARGET
MOC_DIR = $$OBJECTS_DIR
OBJMOC = $$OBJECTS_DIR
RCC_DIR = $$OBJECTS_DIR
QMLDIR = $$PWD
INCLUDEPATH += $$PWD

